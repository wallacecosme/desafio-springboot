package com.compassouol.msproduct.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.compassouol.msproduct.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	
}
