![Logo Compasso](https://compasso.com.br/wp-content/uploads/2020/07/LogoCompasso-Negativo.png)

# Desafio-SpringBoot

Micro serviço de Endpoints da API de produtos.

## Stack usada:

Neste microserviço foi implementado usando a stack descrita abaixo:

Spring Boot,
Banco h2 para realização do seeding de teste
Spring JPA para o mapeamento da entidade.
Spring Validation para validação.
Projeto lombok para geração dos métodos getters, setters, contrutores e etc.
Swagger para documentar os endpoint da API